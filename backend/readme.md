- HTTP STATUS CODE
  100-level (Informational) — Server acknowledges a request
  200-level (Success) — Server completed the request as expected
  300-level (Redirection) — Client needs to perform further actions to complete the request
  400-level (Client error) — Client sent an invalid request
  500-level (Server error) — Server failed to fulfill a valid request due to an error with server

400 Bad Request — Client sent an invalid request — such as lacking required request body or parameter
401 Unauthorized — Client failed to authenticate with the server
403 Forbidden — Client authenticated but does not have permission to access the requested resource
404 Not Found — The requested resource does not exist
412 Precondition Failed — One or more conditions in the request header fields evaluated to false
500 Internal Server Error — A generic error occurred on the server
503 Service Unavailable — The requested service is not available

https://kinsta.com/blog/http-status-codes/#:~:text=200%20Status%20Codes&text=200%3A%20%E2%80%9CEverything%20is%20OK.,has%20created%20a%20new%20resource.
