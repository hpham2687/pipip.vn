/* eslint-disable camelcase */
const { validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const UserModel = require('../models/user.model');
const jwtHelper = require('../helpers/jwt.helper');

const userController = {

  register: async (req, res) => {
    const {
      // eslint-disable-next-line camelcase
      name, phone, email, password, role, referral_code,
    } = req.body;

    // console.log(name, phone, email, password, role, referral_code);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.errors.map((item) => ({
          field: item.param,
          msg: item.msg,
        })),

      });
    }

    // check if user existed
    const user = await UserModel.findUserByEmail(email);

    if (user) {
      return res.status(400).json({ code: 400, msg: 'This email has already registered' });
    }
    const passwordHash = await bcrypt.hash(password, 12);

    // save to database
    const dataNewUser = {
      phone: phone || null,
      email,
      password: passwordHash,
      name,
      // eslint-disable-next-line camelcase
      referral_code: referral_code || null,
      role,
    };
    const newUser = await UserModel.createNew(dataNewUser);
    if (!newUser) {
      throw new Error({ msg: 'Mongodb Error' });
    }
    return res.status(200).json(
      { code: 200, msg: 'Register succesfully' },
    );
  },
  login: async (req, res) => {
    const { email, password } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.errors.map((item) => ({
          field: item.param,
          msg: item.msg,
        })),

      });
    }
    // check if user exist
    const user = await UserModel.findUserByEmail(email);
    if (!user) return res.status(400).json({ msg: 'Cannot find this user' });
    // check password
    const isMatchPassword = await bcrypt.compare(password, user.password);
    if (!isMatchPassword) return res.status(400).json({ msg: 'Password not match' });

    const activationToken = await jwtHelper.generateToken(user, '1h');
    const refreshToken = await jwtHelper.generateToken(user, '7d');
    res.cookie('refresh_token', refreshToken, {
      httpOnly: true,
      path: '/user/refresh_token',
      maxAge: 7 * 24 * 60 * 60 * 1000,
    });

    return res.json({
      msg: 'Login Successfully',
      access_token: activationToken,
      refreshToken,
    });
  },
  refreshToken: async (req, res) => {
    // console.log(req.cookies);
    const { refresh_token } = req.cookies;
    if (!refresh_token) {
      res.json({ msg: 'refresh token cookie required' });
    }
    const decoded = await jwtHelper.verifyToken(refresh_token);

    const accessToken = await jwtHelper.generateToken(decoded.data);
    return res.status(200).json({ accessToken });
  },

};

module.exports = userController;
