const { check } = require('express-validator');

const authValidator = {
  register: [
    check('name').notEmpty().withMessage('Name is required'),
    check('password').notEmpty().withMessage('Password is required').isLength({ min: 6 })
      .withMessage('Password require 6 character length minimun!'),
    check('confirm_password').not().isEmpty().withMessage('Confirm password is required')
      .isLength({ min: 6 })
      .withMessage('Password require 6 character length minimun!'),
    check('referral_code').notEmpty().withMessage('referral_code is required'),
    check('role').notEmpty().withMessage('Role is required')
      .isIn(['passenger', 'driver', 'admin'])
      .withMessage('Role must be "passenger" or "driver"'),

    check('email')
      .notEmpty()
      .withMessage('Email is required')
      .isEmail()
      .withMessage('Email is in wrong format'),
    check('password').custom((value, { req }) => value === req.body.confirm_password).withMessage('Not same'),
  ],
  login: [
    check('email')
      .notEmpty()
      .withMessage('Email is required')
      .isEmail()
      .withMessage('Email is in wrong format'),
    check('password').notEmpty().withMessage('Password is required').isLength({ min: 6 })
      .withMessage('Password require 6 character length minimun!'),
    check('role').notEmpty().withMessage('Role is required')
      .isIn(['passenger', 'driver', 'admin'])
      .withMessage('Role must be "passenger" or "driver"'),

  ],
};

module.exports = authValidator;
