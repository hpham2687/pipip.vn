const express = require('express');
const userController = require('../controllers/user.controller');
const authValidator = require('../middleware/validators/auth.validator');

const userRoute = express.Router();

userRoute.post('/register', authValidator.register, userController.register);
userRoute.post('/login', authValidator.login, userController.login);

userRoute.get('/refresh_token', authValidator.login, userController.refreshToken);

module.exports = userRoute;
