const express = require('express');
const UserRoute = require('./user.route');

const router = express.Router();

/**
 * Init all route
 * @param app from exactly express module
 */

const initRoutes = (app) => {
  app.use('/user', UserRoute);

  // contact route

  return app.use('/', router);
};

module.exports = initRoutes;
