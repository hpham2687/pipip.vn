const mongoose = require('mongoose');

const ROLE = ['passenger', 'driver', 'admin'];
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    default: null,
    required: true,

  },
  password: {
    type: String,
    min: [6, 'Password require 6 character length minimun!'],
    required: true,

  },
  avatar: { type: String, default: null },
  phone: {
    type: String,
    default: null,

  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  referralCode: {
    type: String,
    default: null,
  },
  role: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return ROLE.includes(v);
      },
      message: 'Role must be "passenger" or "driver"ssss',
    },
  },

});
UserSchema.statics = {
  createNew(item) {
    return this.create(item);
  },
  findUserById(id) {
    return this.findOne({ id }).exec();
  },
  findUserByEmail(email) {
    return this.findOne({ email }).exec();
  },

};
UserSchema.methods = {
  comparePassword(password) {
    return (password === this.password);
  },
};

module.exports = mongoose.model('user', UserSchema);
