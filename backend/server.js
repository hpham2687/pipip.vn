require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const connectDb = require('./config/mongodb');
const initRoutes = require('./routes');

const app = express();
const PORT = process.env.PORT || 5000;
// connect mongodb
connectDb();
// allow cors
app.use(cors());
// parse input
app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));

// init routes
initRoutes(app);

app.listen(PORT, () => {
  console.log('listen to ', PORT);
});
