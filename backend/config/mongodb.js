const mongoose = require('mongoose');

// connect to mongo db
const connectDb = () => {
  const DB_NAME = process.env.DB_NAME || 'pippip';
  const DB_PASSWORD = process.env.DB_PASSWORD || 'admin';

  const URI = `mongodb+srv://admin:${DB_PASSWORD}@cluster0.ldf3z.mongodb.net/pippip?retryWrites=true&w=majority`;
  return mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

module.exports = connectDb;
