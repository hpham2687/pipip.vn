require('dotenv').config();

const jwt = require('jsonwebtoken');

/**
 * private function generateToken
 * @param user
 * @param secretSignature
 * @param tokenLife
 */

const accessTokenLife = process.env.JWT_ACCESS_TOKEN_LIFE || '1h';
const generateToken = (user, tokenLife = accessTokenLife) => new Promise((resolve, reject) => {
  const userData = {
    name: user.name,
    password: user.password,
    email: user.email,
  };
  jwt.sign({ data: userData },
    process.env.JWT_ACCESS_TOKEN_SECRET,
    {
      algorithm: 'HS256',
      expiresIn: tokenLife,
    },
    (err, token) => {
      if (err) {
        return reject(err);
      }
      resolve(token);
    });
});
/**
 * This module used for verify jwt token
 * @param {*} token
 * @param {*} secretKey
 */

const verifyToken = (token) => new Promise((resolve, reject) => {
  jwt.verify(token, process.env.JWT_ACCESS_TOKEN_SECRET,
    (err, decoded) => {
      if (err) {
        reject(err);
      }
      resolve(decoded);
    });
});

module.exports = {
  generateToken,
  verifyToken,
};
